import { createRouter, createWebHistory } from 'vue-router'
import Home from '@/views/public/Home.vue'
import Connexion from '@/views/public/Connexion.vue'
import Inscription from '@/views/public/Inscription.vue'
import Test from '@/views/public/Test.vue'
import Modal from '@/views/public/Modal.vue'
import Header from '@/components/Header.vue'
import Footer from '@/components/Footer.vue'
import DashboardAdmin from '@/views/admin/DashboardAdmin.vue'
import Mpo from '@/views/admin/Mpo.vue'
import Connec from '@/views/admin/Connec.vue'
import Regist from '@/views/admin/Regist.vue'

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/header',
    name: 'header',
    component: Header
  },
  {
    path: '/footer',
    name: 'footer',
    component: Footer
  },
  {
    path: '/connexion',
    name: 'connexion',
    component: Connexion
  },
  {
    path: '/inscription',
    name: 'inscription',
    component: Inscription
  },
  {
    path: '/test',
    name: 'test',
    component: Test
  },
  {
    path: '/modal',
    name: 'modal',
    component: Modal
  },
  {
    path: '/dashb',
    name: 'DashboardAdmin',
    component: DashboardAdmin
  },
  {
    path: '/mpo',
    name: 'Mpo',
    component: Mpo
  },
  {
    path: '/connec',
    name: 'Connec',
    component: Connec
  },
  {
    path: '/regist',
    name: 'Regist',
    component: Regist
  },
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
