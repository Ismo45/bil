// Exemple de code serveur avec Express.js

const express = require('express');
const app = express();

// Middleware pour parser le corps de la requête au format JSON
app.use(express.json());

// Point de terminaison pour la connexion
app.post('/api/login', (req, res) => {
  const { username, password } = req.body;

  // Vérification des informations d'identification
  if (username === 'admin' && password === 'password') {
    // Informations d'identification valides
    // Génération d'un jeton d'authentification, gestion de la session, etc.
    res.status(200).json({ message: 'Connexion réussie' });
  } else {
    // Informations d'identification invalides
    res.status(401).json({ message: 'Identifiants invalides' });
  }
});

// Démarrer le serveur
app.listen(3000, () => {
  console.log('Serveur démarré sur le port 3000');
});
